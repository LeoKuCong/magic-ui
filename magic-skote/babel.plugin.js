module.exports = {
  plugins: [
    [
      'import',
      {
        libraryName: '@icon-park/react',
        libraryDirectory: 'es/icons',
        camel2DashComponentName: false,
      },
    ],
  ],
  tailwind: {
    plugins: ['macros'],
    config: './src/tailwind.config.js',
    format: 'auto',
  },
}
