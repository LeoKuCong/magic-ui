module.exports = {
  env: {
    browser: true,
    es2020: true
  },
  extends: [
    'plugin:react/recommended',
    'airbnb'
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 11,
    sourceType: 'module'
  },
  plugins: [
    'react'
  ],
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@', './src'],
          ['@img', './src/assets/images'],
          ['@comp', './src/components'],
        ],
        extensions: ['.ts', '.js', '.jsx', '.json']
      }
    }
  },
  rules: {
    semi: ['error', 'never'],
    'react/self-closing-comp': 0,
    'react/jsx-props-no-spreading': ['error', {
      'exceptions': ['Pagination']
    }],
    'max-len': 0,
    'global-require': 0,
    'no-console': 0,
    'no-underscore-dangle': 0,
    'no-unused-vars': 1,
    'no-nested-ternary': 0,
    'import/no-extraneous-dependencies': 0,
  }
};
