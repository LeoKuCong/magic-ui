import React, { Suspense, lazy } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { store, persistor } from '@/redux/createStore'
import { PersistGate } from 'redux-persist/integration/react'
import { ConfigProvider, Spin } from 'antd'

import './plugins'
import 'antd/dist/antd.less'
import '@icon-park/react/styles/index.less'
import '@/assets/styles/styles.css'
import '@/assets/styles/main.scss'
import zhCN from 'antd/es/locale/zh_CN'
import '@/utils/onlineCheck' // 检测是否联网

import * as serviceWorker from './serviceWorker'

const App = lazy(() => import('./App'))

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Suspense fallback={(
        <div className="fixed top-0 left-0 h-screen w-screen flex items-center justify-center">
          <Spin size="large" tip="页面加载中..." />
        </div>
      )}
      >
        <ConfigProvider locale={zhCN}>
          <App />
        </ConfigProvider>
      </Suspense>
    </PersistGate>
  </Provider>,
  document.getElementById('root'),
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
