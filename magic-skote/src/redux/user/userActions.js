import { SET_USER_INFO } from './userActionTypes'

export const setUserInfo = (payload) => ({ type: SET_USER_INFO, payload })
