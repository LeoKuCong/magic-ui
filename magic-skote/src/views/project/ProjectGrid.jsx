import React, { useEffect, useState } from 'react'
import {
  Spin, Row, Col, Pagination, Avatar, Tooltip, Image, Divider, Tag,
} from 'antd'
import { CalendarDot, Comment } from '@icon-park/react'
import { getProjectList } from '@/api/project'

export default function ProjectGrid() {
  const [tableList, setTableList] = useState([])
  const [gridLoading, setGridLoading] = useState(false)
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 9,
    total: 0,
  })

  async function getProjects(page, pageSize) {
    try {
      setGridLoading(true)
      const { data: { list, pagination: p } } = await getProjectList({
        page, pageSize,
      })
      setPagination(p)
      setTableList(list)
    } finally {
      setGridLoading(false)
    }
  }

  useEffect(() => {
    getProjects()
  }, [])

  const pg = {
    0: { color: 'info', text: '规划中' },
    1: { color: 'primary', text: '实施中' },
    2: { color: 'warning', text: '交付测试' },
    3: { color: 'secondary', text: '已完成' },
  }

  return (
    <>
      <Spin spinning={gridLoading}>
        <Row gutter={[24, 24]}>
          {/* eslint-disable-next-line */
            tableList.slice(0, 9).map(({ id, icon, name, team, date, progress }) => (
              <Col key={id} md={12} lg={8}>
                <div className="bg-white rounded-lg">
                  <div className="px-6 py-4 flex">
                    <Image
                      src={icon}
                      width={100}
                      height={100}
                      fallback="https://gitee.com/chinesee/images/raw/master/magic-ui/img_001.png"
                    />
                    <div className="pl-6">
                      <h2 className="font-bold">{name}</h2>
                      <p className="mt-2 mb-6 text-sm text-gray-600">{name}</p>
                      <Avatar.Group maxCount={3}>
                        {team.map(({ id: userId, name: userName, avatar }) => (
                          <Tooltip key={userId} title={userName}>
                            <Avatar src={avatar} />
                          </Tooltip>
                        ))}
                      </Avatar.Group>
                    </div>
                  </div>
                  <Divider className="m-0" />
                  <div className="px-6 py-4">
                    <ul className="flex items-center text-sm">
                      <li className="mr-4 flex items-center">
                        <Tag color={`rgba(var(--${pg[progress].color}), 1)`}>
                          {pg[progress].text}
                        </Tag>
                      </li>
                      <li className="mr-4 flex items-center">
                        <CalendarDot size={18} className="mr-1" />
                        <span>{date}</span>
                      </li>
                      <li className="mr-4 flex items-center">
                        <Comment size={18} className="mr-1" />
                        <span>229</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </Col>
            ))
          }
        </Row>
      </Spin>

      <div className="mt-10 text-center">
        <Pagination
          {...pagination}
          onChange={getProjectList}
        />
      </div>
    </>
  )
}
