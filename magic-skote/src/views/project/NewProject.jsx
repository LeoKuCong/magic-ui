import React, { useEffect, useState } from 'react'
import {
  Form, Input, Button,
} from 'antd'

export default function NewProject() {
  const onFinish = (values) => {
    console.log('Success:', values)
  }

  return (
    <>
      <Form
        name="newProject"
        labelCol={{ offset: 8, span: 5 }}
        wrapperCol={{ span: 19 }}
        onFinish={onFinish}
      >
        <Form.Item
          label="项目名称"
          name="name"
          rules={[{ required: true, message: '请输入项目名称' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="项目描述"
          name="description"
          rules={[{ required: true, message: '请填写项目描述' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            新建项目
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}
