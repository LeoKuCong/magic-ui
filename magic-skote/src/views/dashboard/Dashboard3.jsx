import React from 'react'
import { useSelector } from 'react-redux'
import {
  Avatar, Button, Menu, Dropdown, Divider, Badge, Radio,
} from 'antd'
import { ArrowRightOutlined } from '@ant-design/icons'
import { More } from '@icon-park/react'

import illustration from '@img/illustration_1.png'
import { ReactComponent as VueIcon } from '@img/icon_vue.svg'
import { ReactComponent as ReactIcon } from '@img/icon_react.svg'
import { ReactComponent as AngularIcon } from '@img/icon_angular.svg'
import styled from 'styled-components'
import tw from 'twin.macro'

import CircleChart from './charts/CircleChart'
import OverviewChart from './charts/OverviewChart'
import ChartCards from './components/ChartCards'

const Row = styled.div`
  ${tw`mb-6 flex`}
`

function DashboardCrypto() {
  const info = useSelector(({ user }) => user.info)

  return (
    <>
      <Row className="mb-6 flex">
        <div className="w-1/3 pr-3">
          <div className="custom-card">
            <div className="mb-6 flex items-center justify-between">
              <Avatar size={40} src={info.avatar} />
              <div>
                <Dropdown
                  overlay={(
                    <Menu className="text-center">
                      <Menu.Item key="0">这是一个操作</Menu.Item>
                      <Menu.Item key="1">这是另一个操作</Menu.Item>
                      <Menu.Item key="2">还是一个操作</Menu.Item>
                    </Menu>
                  )}
                  trigger={['click']}
                >
                  <More className="text-gray-600 cursor-pointer" size={24} />
                </Dropdown>
              </div>
            </div>

            <div className="mt-1 text-base font-bold">华山风清扬</div>
            <div className="my-1 text-sm text-gray-500">czc12580520@gmail.com</div>
            <div className="text-sm text-gray-500">ID: #JK042</div>

            <Divider />

            <div className="flex items-center">
              <div className="flex-1">
                <div>财富余额：</div>
                <div className="text-lg font-bold">￥6908.40</div>
              </div>
              <div className="flex-1">
                <div>本月消费：</div>
                <div className="text-lg font-bold">￥2265.10</div>
              </div>
            </div>

            <Divider />

            <div className="text-center">
              <Button>我的存款</Button>
              <Button className="ml-3" type="primary">本月账单</Button>
            </div>
          </div>
        </div>

        <div className="w-2/3 pl-3">
          <div className="custom-card mb-6 flex items-center justify-between">
            <div>
              <div className="primary font-bold text-lg">欢迎再次访问</div>
              <div className="mt-1 mb-3">Magic UI 数据仪表盘</div>
              <ul className="pl-5 list-disc text-sm text-gray-600">
                {[
                  '朽骨在此相迎，山谷依然',
                  '在此，懵懂的眼睛，初临惊愕',
                  '在此，学会尝试是必修课',
                ].map((text) => (
                  <li key={text} className="li-marker">{text}</li>
                ))}
              </ul>
            </div>

            <img
              className="w-2/5"
              style={{ maxWidth: '250px' }}
              src={illustration}
              alt="插图"
            />
          </div>

          <div className="-mx-3 flex">
            <ChartCards data={
              [
                {
                  icon: VueIcon,
                  name: 'Vue',
                  seriesName: 'Vue',
                  seriesData: [14, 12, 2, 47, 42, 15, 47, 75, 65, 19, 14],
                  color: '#34c38f',
                  count: 8848,
                  value: '+ 254（14.2%）',
                },
                {
                  icon: ReactIcon,
                  name: 'React',
                  seriesName: 'React',
                  seriesData: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54],
                  color: '#50a5f1',
                  count: 4762,
                  value: '+ 128（24.9%）',
                },
                {
                  icon: AngularIcon,
                  name: 'Angular',
                  seriesName: 'Angular',
                  seriesData: [35, 53, 93, 47, 54, 24, 47, 75, 65, 19, 14],
                  color: '#f46a6a',
                  count: 2286,
                  value: '+ 45（2.1%）',
                },
              ]
            }
            />
          </div>
        </div>
      </Row>

      <Row>
        <div className="w-2/3">
          <div className="custom-card">
            <h4 className="custom-card__title">钱包余额</h4>

            <div className="mt-8 flex">
              <div className="w-1/3">
                <div>可用余额</div>
                <div className="mt-2 text-xl font-bold">￥6134.39</div>
                <div className="success">+ 0.12 ( 0.02 % ) </div>
                <div className="mt-5 flex items-center justify-between text-gray-600">
                  <div className="flex-1">
                    <div>收入</div>
                    <div className="mt-1 text-gray-900">￥2632.46</div>
                  </div>
                  <div className="flex-1">
                    <div>支出</div>
                    <div className="mt-1 text-gray-900">￥924.38</div>
                  </div>
                </div>
                <Button
                  className="mt-8 flex items-center"
                  type="primary"
                  size="large"
                >
                  查看更多
                  <ArrowRightOutlined />
                </Button>
              </div>
              <div className="w-1/3" style={{ height: '260px' }}>
                <CircleChart />
              </div>
              <div className="w-1/3">
                <ul className="h-full py-6 pl-8 flex flex-col justify-between">
                  <li className="text-gray-600">
                    <Badge color="#556ee6" text={<span className="text-lg">Ethereum</span>} />
                    <div>4.5701 ETH = $ 1123.64</div>
                  </li>
                  <li className="text-gray-600">
                    <Badge color="#f1b44c" text={<span className="text-lg">Bitcoin</span>} />
                    <div>0.4412 BTC = $ 4025.32</div>
                  </li>
                  <li className="text-gray-600">
                    <Badge color="#50a5f1" text={<span className="text-lg">Litecoin</span>} />
                    <div>35.3811 LTC = $ 2263.09</div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div className="w-1/3 pl-6">
          <div className="custom-card">
            <div className="flex items-center justify-between">
              <h4 className="custom-card__title">概况</h4>
              <Radio.Group value="year">
                <Radio.Button value="month">一月</Radio.Button>
                <Radio.Button value="halfYear">半年</Radio.Button>
                <Radio.Button value="year">一年</Radio.Button>
              </Radio.Group>
            </div>
            <div style={{ height: '280px' }}>
              <OverviewChart />
            </div>
          </div>
        </div>
      </Row>
    </>
  )
}

export default DashboardCrypto
