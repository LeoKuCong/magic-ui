import request from '@/utils/axios'

export function getProjectList(params) {
  return request({
    url: '/project/list',
    method: 'get',
    params,
  })
}

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data,
  })
}
