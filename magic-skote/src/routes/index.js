import { lazy } from 'react'
import { ChartLine, AdProduct, Rss } from '@icon-park/react'

const Dashboard1 = lazy(() => import('@/views/dashboard/Dashboard1'))
const Dashboard2 = lazy(() => import('@/views/dashboard/Dashboard2'))
const Dashboard3 = lazy(() => import('@/views/dashboard/Dashboard3'))
const ProjectList = lazy(() => import('@/views/project/ProjectList'))
const ProjectGrid = lazy(() => import('@/views/project/ProjectGrid'))
const NewProject = lazy(() => import('@/views/project/NewProject'))
const Page = lazy(() => import('@/views/page/Page'))
const Test = lazy(() => import('@/views/test/Test'))

export default [
  {
    title: '数据分析',
    icon: ChartLine,
    children: [
      {
        path: '/index', title: '系统仪表盘', component: Dashboard1,
      },
      {
        path: '/dashboard2', title: '业务仪表盘', component: Dashboard2,
      },
      {
        path: '/dashboard3', title: '数据仪表盘', component: Dashboard3,
      },
    ],
  },

  {
    title: '项目管理',
    icon: AdProduct,
    children: [
      {
        path: '/project-list', title: '项目列表', component: ProjectList,
      },
      {
        path: '/project-grid', title: '项目栅格', component: ProjectGrid,
      },
      {
        path: '/new-project', title: '创建新项目', component: NewProject,
      },
    ],
  },

  {
    path: '/page1', title: '页面1', component: Page, icon: Rss,
  },
  {
    path: '/page2', title: '页面2', component: Page, icon: Rss,
  },
  {
    title: '嵌套菜单',
    icon: Rss,
    children: [
      {
        path: '/page3', title: '页面3', component: Page,
      },
      {
        title: '二级嵌套菜单',
        key: 'menu2',
        children: [{
          path: '/page4', title: '页面4', component: Page,
        }],
      },
    ],
  },
  {
    path: '/test', title: '测试', component: Test, icon: Rss,
  },
]
