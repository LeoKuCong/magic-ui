import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  Switch, Route, Redirect, useHistory, useLocation,
} from 'react-router-dom'
import Helmet from 'react-helmet'
import { useDispatch } from 'react-redux'
import { signOut } from '@/redux/app/appActions'
import { getToken } from '@/utils/token'

function createRoutes(routes) {
  return routes.map(({
    path, title, exact, children, component: Component,
  }) => {
    if (children?.length > 0) {
      return createRoutes(children)
    }
    return (
      <Route key={path} exact={exact || false} path={path}>
        <Helmet>
          <title>
            {title ? `${title} - ${process.env.REACT_APP_PAGE_TITLE}` : process.env.REACT_APP_PAGE_TITLE}
          </title>
        </Helmet>
        <Component />
      </Route>
    )
  })
}

function AppContent({ routes }) {
  const dispatch = useDispatch()
  const location = useLocation()
  const history = useHistory()

  useEffect(() => {
    if (!getToken()) {
      dispatch(signOut())
      history.replace('/')
    }
  }, [location, dispatch, history])

  return (
    <Switch>
      {createRoutes(routes)}
      <Redirect to="/404" />
    </Switch>
  )
}

AppContent.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default AppContent
