import Mock from 'mockjs'

Mock.mock('/user/login', 'post', { code: 2000, data: { token: 'token' } })
Mock.mock('/user/logout', 'post', { code: 2000 })
Mock.mock('/user/info', 'get', { code: 2000 })

Mock.setup({
  timeout: '300-500',
})
