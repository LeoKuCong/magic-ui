/**
 * Redis管理
 */

import request from '@/plugins/axios'

export function getKeyList(params) {
  return request({
    url: '/redis/getList',
    method: 'get',
    params,
  })
}

export function getValueByKey(data) {
  return request({
    url: '/redis/getByKey',
    method: 'post',
    data,
  })
}

export function deleteValueByKey(data) {
  return request({
    url: '/redis/deleteByKey',
    method: 'post',
    data,
  })
}

export function deleteByKeyField(data) {
  return request({
    url: '/redis/deleteByKeyField',
    method: 'post',
    data,
  })
}
