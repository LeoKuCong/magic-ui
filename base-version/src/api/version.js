/**
 * 历史版本
 */

import request from '@/plugins/axios'

export function getVersionList(params) {
  return request({
    url: '/version/getList',
    method: 'get',
    params,
  })
}

export function createVersion(data) {
  return request({
    url: '/version/create',
    method: 'post',
    data,
  })
}

export function updateVersion(data) {
  return request({
    url: '/version/update',
    method: 'post',
    data,
  })
}

export function deleteVersion(data) {
  return request({
    url: '/version/delete',
    method: 'post',
    data,
  })
}
