import Vue from 'vue'
import VueRouter from 'vue-router'
import MainLayout from '@/layouts/MainLayout.vue'
import UserLayout from '@/layouts/UserLayout.vue'

Vue.use(VueRouter)

export const routes = [
  {
    path: '',
    component: MainLayout,
    meta: { title: '定时任务管理', icon: 'tag' },
    children: [
      {
        path: '/task-log',
        name: 'TaskLog',
        component: () => import(/* webpackChunkName: 'TaskLog' */ '@/views/task/TaskLog.vue'),
        meta: { title: '运行日志' },
      },
    ],
  },

  {
    path: '/',
    redirect: '/version',
    single: true,
    component: MainLayout,
    children: [
      {
        path: 'version',
        name: 'Version',
        component: () => import(/* webpackChunkName: 'Version' */ '@/views/version/Version.vue'),
        meta: { title: '历史版本', icon: 'tag' },
      },
    ],
  },

  {
    path: '',
    hidden: true,
    component: MainLayout,
    children: [
      {
        path: '/profile',
        name: 'Profile',
        component: () => import(/* webpackChunkName: 'Profile' */ '@/views/user/Profile.vue'),
        meta: { title: '个人中心' },
      },
    ],
  },

  {
    path: '',
    hidden: true,
    component: MainLayout,
    children: [
      {
        path: '/test',
        name: 'Test',
        component: () => import('@/views/Test.vue'),
        meta: { title: '测试' },
      },
    ],
  },

  {
    path: '/user',
    redirect: '/user/login',
    component: UserLayout,
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'Login',
        components: {
          login: () => import(/* webpackChunkName: 'Login' */ '@/views/user/Login.vue'),
        },
        meta: { title: '登录' },
      },
      {
        path: 'register',
        name: 'Register',
        components: {
          register: () => import(/* webpackChunkName: 'Register' */ '@/views/user/Register.vue'),
        },
        meta: { title: '注册' },
      },
    ],
  },

  {
    path: '/not-found',
    component: () => import(/* webpackChunkName: 'NotFound' */ '@/views/error-page/NotFound.vue'),
  },

  { path: '*', redirect: '/not-found', hidden: true },
]

const router = new VueRouter({
  mode: process.env.VUE_APP_ROUTER_MODE,
  base: process.env.BASE_URL,
  routes,
})

export default router
