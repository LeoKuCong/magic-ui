const mutations = {
  SET_SIDE_MENU_STATUS(state, status) {
    state.isSideMenuOpened = status
  },
}

export default mutations
