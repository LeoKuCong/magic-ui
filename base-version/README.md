# Magic 后台管理系统模板

## 项目设置
```
npm ci
or
npm install
```

### 开发环境下运行项目
```
npm run serve
```

### 构建生产环境
```
npm run build
```

### 格式化和修复代码文件
```
npm run lint
```

### 自定义配置
查看 [Vue CLI 配置参考](https://cli.vuejs.org/zh/config/).
