/**
 * 公共接口
 */

import request from '@/plugins/axios'

export function getMenu(params) {
  return request({
    url: '/menu/getMenu',
    method: 'get',
    params,
  })
}

export function login(data) {
  return request({
    url: '/login/login',
    method: 'post',
    data,
  })
}

export function logout(data) {
  return request({
    url: '/login/logout',
    method: 'post',
    data,
  })
}

export function getPublicKey() {
  return request({
    url: '/login/getPublicKey',
    method: 'get',
  })
}
