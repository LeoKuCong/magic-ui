/**
 * 运行日志管理
 */

import request from '@/plugins/axios'

export function sqlAnalysis(data) {
  return request({
    url: '/soar/slowAnalysis',
    method: 'post',
    data,
  })
}

export function getSqlFrequencyList(params) {
  return request({
    url: '/sql/getFrequencyList',
    method: 'get',
    params,
  })
}

export function getLogList(params) {
  return request({
    url: '/logFile/getList',
    method: 'get',
    params,
  })
}

export function downLog(params) {
  return request({
    url: '/logFile/downLog',
    method: 'get',
    responseType: 'blob',
    params,
  })
}

export function getOperationLogList(params) {
  return request({
    url: '/log/getList',
    method: 'get',
    params,
  })
}
