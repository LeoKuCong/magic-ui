import NProgress from 'nprogress'
import router from './router'

import 'nprogress/nprogress.css'

NProgress.configure({ showSpinner: false }) // NProgress 配置

const whiteList = ['Login']

function setPageTitle(title) {
  const subTitle = title ? `${title} - ` : ''
  document.title = `${subTitle}${process.env.VUE_APP_PAGE_TITLE}`
}

router.beforeEach((to, from, next) => {
  NProgress.start()

  setPageTitle(to.meta?.title)

  const flag = true
  if (flag) {
    if (to.name === 'Login') {
      // 如果已经有了 token 再访问登录页的话，将会被重定向到首页
      next('/')
    } else {
      next()
    }
  } else if (whiteList.includes(to.name)) {
    // 如果路由在白名单里面，直接 next
    next()
  } else {
    // 如果没有权限，则重定向回登录页
    next('/login')
  }
})

router.afterEach(() => {
  NProgress.done()
})
