import Vue from 'vue'

import '@/assets/scss/tailwind.scss'
import './ant-design'
import VueFeather from 'vue-feather'
import Storage from './storage'
import OnlineCheck from './online-check'
import 'dayjs/locale/zh-cn'

Vue.use(VueFeather)
Vue.use(Storage)
Vue.use(OnlineCheck)
