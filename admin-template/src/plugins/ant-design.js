import Vue from 'vue'
import {
  Button, ConfigProvider, Dropdown, DatePicker, Drawer, Form,
  Input,
  message, Menu, Modal, notification, Popconfirm, Result, Spin,
  Table, Tag,
} from 'ant-design-vue'

Vue.config.productionTip = false

Vue.use(Button)
Vue.use(ConfigProvider)
Vue.use(Dropdown)
Vue.use(DatePicker)
Vue.use(Drawer)
Vue.use(Form)
Vue.use(Input)
Vue.use(Menu)
Vue.use(Modal)
Vue.use(Popconfirm)
Vue.use(Result)
Vue.use(Spin)
Vue.use(Table)
Vue.use(Tag)

Vue.prototype.$message = message
Vue.prototype.$notify = notification
Vue.prototype.$info = Modal.info
Vue.prototype.$confirm = Modal.confirm

// 完成组件列表
// Base,
// version,
// install,
// message,
// notification,
// Affix,
// Anchor,
// AutoComplete,
// Alert,
// Avatar,
// BackTop,
// Badge,
// Breadcrumb,
// Button,
// Calendar,
// Card,
// Collapse,
// Carousel,
// Cascader,
// Checkbox,
// Col,
// DatePicker,
// Divider,
// Dropdown,
// Form,
// FormModel,
// Icon,
// Input,
// InputNumber,
// Layout,
// List,
// LocaleProvider,
// Menu,
// Mentions,
// Modal,
// Pagination,
// Popconfirm,
// Popover,
// Progress,
// Radio,
// Rate,
// Row,
// Select,
// Slider,
// Spin,
// Statistic,
// Steps,
// Switch,
// Table,
// Transfer,
// Tree,
// TreeSelect,
// Tabs,
// Tag,
// TimePicker,
// Timeline,
// Tooltip,
// Upload,
// Drawer,
// Skeleton,
// Comment,
// ConfigProvider,
// Empty,
// Result,
// Descriptions,
// PageHeader,
