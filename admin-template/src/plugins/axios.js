import axios from 'axios'
import Router from '@/router/router'
import Message from 'ant-design-vue/es/message'
import responseHandler from '@/utils/response-handler'

const errorHandler = (error) => {
  const { status = 'default', statusText = '错误信息' } = error.response
  responseHandler[status](status, statusText)

  return Promise.reject(error)
}

const service = axios.create({
  baseURL: process.env.VUE_APP_REQUEST_BASE_URL,
  timeout: 20000,
  responseType: 'json',
  withCredentials: true,
})

service.interceptors.request.use(
  (config) => config,
  errorHandler,
)

service.interceptors.response.use(
  (response) => {
    const { data } = response

    const { code, success, message } = data
    if (code && !success) {
      Message.warning(message)

      if (code === '002') {
        Router.replace({ name: 'Login' }, () => { })
      }

      return Promise.reject(data)
    }
    return data
  },
  errorHandler,
)

export default service
