import { login, logout } from '@/api/common'
import { getUserInfo } from '@/api/user'

const state = {
  info: {},
}

const mutations = {
  SET_USER_INFO(state, info) {
    state.info = info
  },
}

const actions = {
  async login({ commit }, { account, password }) {
    try {
      const { success } = await login({ account, password })
      if (success) {
        const { data } = await getUserInfo()
        commit('SET_USER_INFO', data)
        return true
      }
      return false
    } catch {
      return false
    }
  },

  async logout() {
    try {
      await logout()
      setLoginStatus(false)
      return true
    } catch {
      return false
    }
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters: {},
}
