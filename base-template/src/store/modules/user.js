import { setToken } from '@/utils/token'
import { login, logout, getUserInfo } from '@/api/user'

const state = {
  info: {},
}

const mutations = {
  SET_USER_INFO(state, info) {
    state.info = info
  },
}

const actions = {
  async login({ commit }, { account, password }) {
    try {
      const { data: { token } } = await login({ account, password })
      const { data } = await getUserInfo()
      commit('SET_USER_INFO', data)
      setToken(token)
      return true
    } catch {
      return false
    }
  },

  async logout() {
    try {
      await logout()
      setToken(false)
      return true
    } catch {
      return false
    }
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters: {},
}
