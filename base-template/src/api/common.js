/**
 * 公共接口
 */

import request from '@/plugins/axios'

export function getMenu(params) {
  return request({
    url: '/menu/getMenu',
    method: 'get',
    params,
  })
}

export function getPublicKey() {
  return request({
    url: '/login/getPublicKey',
    method: 'get',
  })
}
