import Vue from 'vue'

import '@/assets/scss/tailwind.scss'
import '@/mock'
import './ant-design'
import Storage from './storage'
import OnlineCheck from './online-check'
import 'dayjs/locale/zh-cn'

Vue.use(Storage)
Vue.use(OnlineCheck)
