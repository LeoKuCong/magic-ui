const AntdDayjsWebpackPlugin = require('antd-dayjs-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

const IS_PROD = process.env.NODE_ENV === 'production'

const assetsCDN = {
  externals: {
    vue: 'Vue',
    'vue-router': 'VueRouter',
    vuex: 'Vuex',
    axios: 'axios',
  },
  css: [],
  js: [
    '//cdn.jsdelivr.net/npm/vue@2.6.11/dist/vue.min.js',
    '//cdn.jsdelivr.net/npm/vue-router@3.1.6/dist/vue-router.min.js',
    '//cdn.jsdelivr.net/npm/vuex@3.1.3/dist/vuex.min.js',
    '//cdn.jsdelivr.net/npm/axios@0.19.2/dist/axios.min.js',
  ],
}

module.exports = {
  productionSourceMap: false,

  devServer: {
    host: '0.0.0.0',
    proxy: {
      [process.env.VUE_APP_REQUEST_BASE_URL]: {
        target: process.env.VUE_APP_BASE_API,
        changeOrigin: true,
      },
    },
  },

  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            'primary-color': '#6485ff',
            'success-color': '#46c93a',
            'warning-color': '#ffba00',
            'error-color': '#ff4757',
            'info-color': '#6485ff',
            'processing-color': '#6485ff',
            'border-radius-base': '6px',
          },
          javascriptEnabled: true,
        },
      },
      sass: { prependData: '@import "@/assets/scss/variables.scss";' },
    },
  },

  chainWebpack: (config) => {
    config.resolve.alias
      .set('@img', '@/assets/images')
      .set('@comp', '@/components')

    config
      .plugin('html')
      .tap((args) => {
        args[0].title = process.env.VUE_APP_PAGE_TITLE
        if (IS_PROD) {
          args[0].cdn = assetsCDN
        }
        return args
      })
  },

  configureWebpack: (config) => {
    config.plugins.push(
      new AntdDayjsWebpackPlugin({
        preset: 'antdv3',
      }),
    )
    config.optimization = {
      splitChunks: {
        chunks: 'all',
      },
    }

    if (IS_PROD) {
      config.plugins.push(
        new CompressionPlugin({
          test: /\.js$|\.html$|\.css/,
          threshold: 8192,
        }),
      )

      config.externals = {
        ...config.externals,
        ...assetsCDN.externals,
      }
    }

    if (process.env.VUE_APP_MODE === 'preview') {
      config.plugins.push(
        new BundleAnalyzerPlugin(),
      )
    }
  },
}
