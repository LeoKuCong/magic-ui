module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
  ],
  important: true,
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
