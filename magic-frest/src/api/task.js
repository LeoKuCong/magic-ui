/**
 * 定时任务管理
 */

import request from '@/plugins/axios'

export function getTaskLogList(params) {
  return request({
    url: '/CrontabLog/getList',
    method: 'get',
    params,
  })
}

export function getTaskRunList(params) {
  return request({
    url: '/taskRun/getList',
    method: 'get',
    params,
  })
}

export function deliveryTask(params) {
  return request({
    url: '/taskRun/delivery',
    method: 'get',
    params,
  })
}
