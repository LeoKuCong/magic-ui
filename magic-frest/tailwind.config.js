module.exports = {
  important: true,
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
  ],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
